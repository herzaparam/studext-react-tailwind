module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        gold: '#FFCE00',
        lightGold: '#FFEEA9',
        silver: '#D1D1C2',
        lightSilver: '#EFEFEF',
        bronze: '#EBA468',
        lightBronze: '#FDDDC2',
      }
    },
    fontFamily: {
      'body': ['Quicksand', 'sans-serif'],
     }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
