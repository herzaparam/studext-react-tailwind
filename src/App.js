import './App.css';
import Navbar from '../src/components/module/Navbar'
import Stories from '../src/components/module/Stories'
import Statistics from '../src/components/module/Statistics'
import Ranks from '../src/components/module/Ranks'
import Course from './components/module/Course';

function App() {
  return (
    <div className="h-screen flex flex-col">
      <Navbar />

      <main className="flex flex-auto flex-col md:flex-row">
        <div className="flex flex-col flex-auto">
          <Stories />
          <Statistics />
        </div>
        <Ranks />
      </main>
      <Course />
    </div>
  );
}

export default App;
