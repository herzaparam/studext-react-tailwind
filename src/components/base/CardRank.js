import React from 'react'

import { ProfileDummy } from '../../images'

function CardRank({ position, name }) {
    return (
        <div className={`flex flex-none items-center w-auto shadow-md rounded-md px-4 mb-2 ${position === "1" ? "h-16 bg-gradient-to-r from-gold to-lightGold" : position === "2" ? "h-14 bg-gradient-to-r from-silver to-lightSilver" : position === "3" ? "h-14 bg-gradient-to-r from-bronze to-lightBronze" : "h-14 bg-gray-50"}`} >
            <p className="bg-white rounded-full w-4 h-4 leading-4 text-xs md:text-base md:w-6 md:h-6 text-center md:leading-6 font-bold flex-none">{position}</p>
            <img src={ProfileDummy} alt="" className="h-9 w-9 mx-4 object-cover rounded-full flex-none" />
            <h4 className="font-bold flex-auto text-lg">{name.length > 6 ? `${name.substring(0, 6)}...` : name}</h4>
            <div className="flex justify-evenly items-center mx-2">
                <h5 className="bg-white rounded-full w-5 h-5 leading-5 md:w-7 md:h-7 mx-1 md:leading-7 text-center font-bold text-xs">100</h5>
                <h5 className="bg-white rounded-full w-5 h-5 leading-5 md:w-7 md:h-7 mx-1 md:leading-7 text-center font-bold text-xs">90</h5>
                <h5 className="bg-white rounded-full w-5 h-5 leading-5 md:w-7 md:h-7 mx-1 md:leading-7 text-center font-bold text-xs">80</h5>
            </div>
            <h5 className="font-bold flex-none">100</h5>
        </div>
    )
}

export default CardRank
