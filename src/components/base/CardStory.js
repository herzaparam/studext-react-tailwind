import React from 'react'

import { ProfileDummy } from '../../images'

function CardStory({ name, image }) {
    return (
        <div className="flex flex-col mr-5">
            <div className="w-12 h-12 rounded-full bg-gradient-to-r from-blue-600 via-green-400 to-blue-500 flex justify-center items-center">
                <img src={ProfileDummy} alt="" className="w-11 h-11 rounded-full object-cover border-2 border-white" />
            </div>
            <p className="font-light text-xs text-center mt-1">{name.length > 6 ? `${name.substring(0, 6)}...` : name}</p>
        </div>
    )
}

export default CardStory
