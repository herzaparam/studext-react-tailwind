import React from 'react'
import CardCourse from '../base/CardCourse'
import HorizontalScroll from 'react-scroll-horizontal'


function Course() {
    return (
        <div className="shadow-lg flex-none w-auto h-1/3 px-3 pt-3 pb-1 m-4 shadow-xl">
            <h3 className="font-bold text-xl">Course Preview</h3>
            <HorizontalScroll reverseScroll = { true }>
                <CardCourse />
                <CardCourse />
                <CardCourse />
                <CardCourse />
                <CardCourse />
                <CardCourse />
            </HorizontalScroll>
        </div>
    )
}

export default Course
