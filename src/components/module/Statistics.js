import React from 'react'

import Chart from '../base/Chart'

function Statistics() {
    return (
        <div className="mx-4 shadow-lg bg-white rounded-xl px-3 pt-3 pb-1 font-bold text-xl flex-auto flex-none max-h-48">
            <h3 className="font-bold">Statistic</h3>
            <Chart />
        </div>
    )
}

export default Statistics
