import React from 'react'

import CardRank from '../base/CardRank'
import { trophy, confetty } from '../../images'

function Ranks() {
    return (
        <div className="flex flex-col flex-auto md:w-2/5 flex-none m-4 shadow-lg bg-white rounded-xl px-3 py-3 relative bg-repeat-x" style={{backgroundImage: `url(${confetty})` }}>
            <h3 className="font-bold text-xl">Peringkat</h3>
            <div className="">
                <p>Hasil akhir pemerolehan nilai try out</p>
                <button className="flex items-center font-bold text-blue-400 my-1">
                    <svg className="" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M3 12V12.75C3 13.3467 3.23705 13.919 3.65901 14.341C4.08097 14.7629 4.65326 15 5.25 15H12.75C13.3467 15 13.919 14.7629 14.341 14.341C14.7629 13.919 15 13.3467 15 12.75V12M12 9L9 12M9 12L6 9M9 12V3" stroke="#008FFF" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                    Unduh .CSV
                </button>
                <img src={trophy} alt="" className="absolute top-3 right-0" />
            </div>
            <div className="flex flex-col overflow-y-auto overscroll-contain h-52 z-10 bg-white">
                <CardRank position="1" name="Herza Paramayudhanto"/>
                <CardRank position="2" name="Sebastian"/>
                <CardRank position="3" name="Lionel Messi"/>
                <CardRank position="4" name="Cristiano Ronaldo Aleksander Graham Bell"/>
                <CardRank position="5" name="Johan Sundstein"/>
                <CardRank position="6" name="Amer"/>
            </div>
        </div>
    )
}

export default Ranks
