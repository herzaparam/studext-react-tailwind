import React, { useEffect, useState } from 'react'
import CardStory from '../base/CardStory'
import HorizontalScroll from 'react-scroll-horizontal'
import axios from 'axios'

function Stories() {

    const [users, setUsers] = useState([])

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/comments')
            .then((res) => {
                setUsers(res.data)
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])

    return (
        <div className="shadow-lg m-4 bg-white rounded-xl px-3 pt-3 pb-1 font-bold text-xl h-28 md:h-2/6 flex-none">
            <h3 className="font-bold">Story</h3>
            <HorizontalScroll reverseScroll={true} >
                {users.map((user) => {
                    return (
                        <CardStory name={user.name} key={user.id} />
                    )
                })}

            </HorizontalScroll>
        </div>
    )
}

export default Stories
