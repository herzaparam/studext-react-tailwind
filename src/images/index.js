import ProfileDummy from './ceb.jpg'
import confetty from './confetty.png'
import iconChat from './icon chat.png'
import iconDashboard from './icon dashboard.png'
import iconDownload from './icon download.png'
import iconHome from './icon home.png'
import iconCourse from './icon my course.png'
import iconNotif from './icon notification.png'
import iconSearch from './icon search.png'
import iconSmile from './icon smile.png'
import iconBadge from './Rectangle 367.png'
import courseDummy from './Rectangle 356.png'
import iconPerson from './Profile.png'
import trophy from './throphy.png'
import logoStudext from './logo studext 1.png'


export {
    ProfileDummy,
    iconBadge,
    iconSmile,
    iconNotif,
    iconSearch,
    iconPerson,
    iconHome,
    iconChat,
    iconDashboard,
    iconDownload,
    iconCourse,
    confetty,
    logoStudext,
    trophy,
    courseDummy
}